import unittest
from values import strings
from helpers.webdriver import DriverInit
from pageobject.home.productList import ProductListPage
from pageobject.home.home import LoginPage
import time


class EbayTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = DriverInit.driver_setup(self, 'local_firefox')
        self.lg = LoginPage(self.driver)
        self.productList = ProductListPage(self.driver)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_A_visit_page(self):
        self.driver.get(strings.base_url)
        self.lg.enterSearch("shoes")
        self.lg.clickSearchButton()

    def test_B_apply_filters(self):
        self.productList.enterSearch("puma")
        self.productList.checkBox()
        self.productList.checkEstado()

    def test_C_obtain_total_results(self):
        print(self.productList.getresultsCount())


    def test_D_order_result(self):
        self.productList.clickResultSort()
        time.sleep(2)
        self.productList.selectResultSort()


    def test_E_present_products(self):
        #self.productList.clickResultSort()
        time.sleep(2)
        self.productList.selectProductResultSet(5)

    def test_F_sort_products_by_name_asc(self):
        self.productList.orderProductNamesAsc()

    def test_G_sort_products_by_price_desc(self):
        self.productList.orderProductPricesDesc()


if __name__ == "__main__":
    unittest.main()



