import unittest
import HtmlTestRunner
from helpers.mail import EmailCreation
from testcases.ebay_test_cases import EbayTest
import time
import sys


program_name = sys.argv[0]
arguments = sys.argv[1:]
count = len(arguments)



html_report_dir = './html_report'

# Get all tests from the test classes
tc1 = unittest.TestLoader().loadTestsFromTestCase(EbayTest)


# Create a test suite combining all test classes
fullEbayFlow = unittest.TestSuite([tc1])

test_runner = HtmlTestRunner.HTMLTestRunner(combine_reports=True, output=html_report_dir, verbosity=1, report_name="ebay_report", add_timestamp=False)
test_runner.run(fullEbayFlow)

time.sleep(5)

contents = ""
with open(html_report_dir+'/ebay_report.html') as f:
    for line in f.readlines():
        contents += line
EmailCreation.sendEmail(contents+'>', arguments[0])

