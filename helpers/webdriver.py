from selenium import webdriver
import subprocess
import os.path


class DriverInit:


    def driver_setup(self, browser):

        my_path = os.path.abspath(os.path.dirname(__file__))
        pathChrome = os.path.join(my_path, "drivers/") + "chromedriver"
        pathFirefox = os.path.join(my_path,"drivers/") + "geckodriver"

        if browser == 'chrome':
            self.driver = webdriver.Remote(
                command_executor='http://localhost:4444/wd/hub',
                desired_capabilities={'browserName': 'chrome', 'javascriptEnabled': True})
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)
        if browser == 'firefox':
            self.driver = webdriver.Remote(
                command_executor='http://localhost:4444/wd/hub',
                desired_capabilities={'browserName': 'firefox', 'javascriptEnabled': True})
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)

        if browser == 'local_firefox':
            self.driver = webdriver.Firefox(executable_path=pathFirefox)
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)

        else:
            self.driver = webdriver.Chrome(executable_path=pathChrome)
            self.driver.maximize_window()
            self.driver.implicitly_wait(10)

        return self.driver