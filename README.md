# Test Automation Framework

Automation framework that manage the test cases using the object model pattern

## Installation

For Local Execution

Install the package-management system pip

```bash
sudo apt-get install python3-pip
```
After that install virtualenv

```bash
sudo pip3 install virtualenv 
```

Create a virtualenv for the execution

```bash
virtualenv env 
```
Activate the virtualenv

```bash
source venv/bin/activate
```
Install the necessary requirements of the project using the command
```bash
pip install -r requirements.txt
```

In the root of the project export the environment variable PYTHONPATH with the following command:

```bash
export PYTHONPATH=$PYTHONPATH:$PWD
```

For Docker Execution

Install docker and docker-compose, in order to do that follow the following doc's:
 
https://docs.docker.com/install/linux/docker-ce/ubuntu/

https://docs.docker.com/compose/install/

To initialize the docker-compose project run the following command in the root of the project

```bash
docker-compose build
```
After it finish start the dockers images running:
```bash
docker-compose up
```

## Usage

For run the script directly in the console run the following

```bash
python testcases/test_suite.py <email>
```


The project allows the execution using the local environment, in order to do that change the setup class in the testcases.ebay_test_cases.py file, this allows to run the project on local or in selenium hub using the following flags in the setUpClass.

- local_firefox
- local_chrome
- firefox
- chrome

```python
    @classmethod
    def setUpClass(self):
        self.driver = DriverInit.driver_setup(self, 'local_firefox')
        self.lg = LoginPage(self.driver)
        self.productList = ProductListPage(self.driver)
```


Also if you want to check the status of the selenium hub enter in a browser to the address


```html
http://localhost:4444/grid/console
```


Finally the report it’s presented also in a web application that runs in local, this only works if the docker-compose it’s running and it will show the last execution of the test, for check that go to


```html
http://localhost:5000/ebay_report/
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)