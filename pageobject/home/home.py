from base.selenium_driver import SeleniumDriver



class LoginPage(SeleniumDriver):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #Locators
    __selectEnglish = "//span[contains(text(),'English')]"
    _searchBox = "gh-ac"
    _searchButton = "gh-btn"

    def enterSearch(self,search):
        SeleniumDriver.sendKeys(self, search,self._searchBox, locatorType="id")

    def clickSearchButton(self):
        SeleniumDriver.elementClick(self, self._searchButton,locatorType="id")

