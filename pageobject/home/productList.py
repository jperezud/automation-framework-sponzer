from selenium.webdriver.common.keys import Keys
from base.selenium_driver import SeleniumDriver


class ProductListPage(SeleniumDriver):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Locators
    _searchBrand = "w3-w12-0[0]"
    _brandComboBox = "//span[@class='cbx x-refine__multi-select-cbx'][contains(text(),'PUMA')]"
    _stateComboBox = "//span[@class='cbx x-refine__multi-select-cbx'][text()='Nuevo']"
    _resultsCount = "//h1[@class='srp-controls__count-heading']"
    _sortSelector = "//div[@id='w4-w3']//button"
    _sortPriceAss = "//div[@id='w4-w3']//div[@class='x-flyout__content flyout-notice flyout-notice--information flyout-notice--top-right']/div/ul/li[5]/a"
    _totalResults = "//ul[@class='srp-results srp-grid clearfix']/li"
    _priceResultSet = "//span[@class='s-item__price']"
    _namesResultSet = "//h3[@class='s-item__title']"

    def enterSearch(self,search):
        SeleniumDriver.sendKeys(self, search + Keys.RETURN ,self._searchBrand, locatorType="id")

    def checkBox(self):
        SeleniumDriver.elementClick(self,self._brandComboBox ,locatorType="xpath")

    def checkEstado(self):
        SeleniumDriver.srollDown(self,1)
        SeleniumDriver.elementClick(self,self._stateComboBox, locatorType="xpath")

    def getresultsCount(self):
        count = SeleniumDriver.getText(self,self._resultsCount, locatorType="xpath")
        return count

    def selectResultSort(self):
        sel = SeleniumDriver.getElement(self, self._sortSelector, locatorType="xpath")
        SeleniumDriver.moveOver(self, sel)
        SeleniumDriver.elementClick(self, self._sortPriceAss, locatorType="xpath")

    def clickResultSort(self):
        SeleniumDriver.elementClick(self, self._sortSelector,locatorType="xpath")

    def selectProductResultSet(self,size):
        priceList = SeleniumDriver.getElementList(self, self._priceResultSet, locatorType="xpath")
        namesList = SeleniumDriver.getElementList(self,self._namesResultSet, locatorType='xpath')
        i=0
        A = list(zip(namesList, priceList))

        for product in A:
            print("PRODUCTO: "+product[0].text + " PRECIO: "+ product[1].text + "<br>")
            i = i + 1
            if i == size:
                break
        return A

    def orderProductPricesDesc(self):
        priceList = SeleniumDriver.getElementList(self,self._priceResultSet, locatorType='xpath')
        prices = []
        for price in priceList:
            prices.append(price.text)

        for pr in sorted(prices, reverse=True):
            print(pr + "<br>")

    def orderProductNamesAsc(self):
        namesList = SeleniumDriver.getElementList(self,self._namesResultSet, locatorType='xpath')
        names = []
        for name in namesList:
            names.append(name.text)

        for nam in sorted(names):
            print(nam + "<br>")

    def orderPriceList(self,size):
        priceList = SeleniumDriver.getElementList(self, self._priceResultSet, locatorType="xpath")
        flag = False
        i = 1
        while i < len(priceList):
            if (priceList[i].text < priceList[i - 1].text):
                print(priceList[i].text + "<br>")
                flag = True
            i += 1
        if (flag == True):
            print("Yes, List is sorted.")
        else:
            print("No, List is not sorted.")
        return flag







































